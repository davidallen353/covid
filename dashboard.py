import numpy as np
import streamlit as st
import matplotlib.pyplot as plt

import covid.stats as stats

from covid.population import Population
from covid.info import INFECTION_STATES, PROGRESS_PARAMETERS

simulation_length = st.sidebar.slider('Simulation Length', min_value=5, max_value=1000, value=100, step=1)
grid_size = st.sidebar.slider('Grid Size', min_value=5, max_value=200, value=50, step=1)
population_size = st.sidebar.slider('Population', min_value=10, max_value=grid_size**2, value=grid_size**2, step=1)

movement = st.sidebar.slider('Distance Travelled', min_value = 0.1, max_value = 3.0, value = 1.0)

probability_symptomatic = st.sidebar.slider('Probabilty Symptoms Develop', value=PROGRESS_PARAMETERS['probability_symptomatic'])
probability_critical = st.sidebar.slider('Probability of Critical Symptoms', value=PROGRESS_PARAMETERS['probability_critical'])
probability_death = st.sidebar.slider('Probability of Death', value=PROGRESS_PARAMETERS['probability_death'])
max_days_asymptomatic = st.sidebar.slider('Max days asymptomatic', value=PROGRESS_PARAMETERS['max_days_asymptomatic'])
max_days_symptomatic = st.sidebar.slider('Max days symptomatic', value=PROGRESS_PARAMETERS['max_days_symptomatic'])
max_days_critical = st.sidebar.slider('Max days critical', value=PROGRESS_PARAMETERS['max_days_critical'])
max_days_recovery = st.sidebar.slider('Max days recovery', value=PROGRESS_PARAMETERS['max_days_recovery'])
average_days_immune = st.sidebar.slider('Average Immunity Length', min_value=1, max_value=1000, value=PROGRESS_PARAMETERS['average_days_immune'])

PROGRESS_PARAMETERS['probability_symptomatic'] = probability_symptomatic
PROGRESS_PARAMETERS['probability_critical'] = probability_critical
PROGRESS_PARAMETERS['probability_death'] = probability_death
PROGRESS_PARAMETERS['max_days_asymptomatic'] = max_days_asymptomatic
PROGRESS_PARAMETERS['max_days_symptomatic'] = max_days_symptomatic
PROGRESS_PARAMETERS['max_days_critical'] = max_days_critical
PROGRESS_PARAMETERS['max_days_recovery'] = max_days_recovery
PROGRESS_PARAMETERS['average_days_immune'] = average_days_immune

population = Population(grid_size)
population.remove_agents(grid_size**2 - population_size)

max_days = simulation_length + 1

bar = st.progress(0)

population.start_infection(10)

status_history = {_status: [] for _status in range(7)}

latest_iteration_vulnerable = st.empty()
latest_iteration_symptomatic = st.empty()
latest_iteration_mortality = st.empty()

for day in range(max_days):
    bar.progress(day / simulation_length)
    population.advance_day(movement)

    status_summary = population.get_status_summary()

    for _status in status_summary:
        status_history[_status].append(status_summary[_status]) 

    _pop = stats.get_population_size(status_summary)
    _vulnerable = stats.get_number_vulnerable(status_summary)
    _symptomatic = stats.get_number_symptomatic(status_summary)
    _deaths = stats.get_number_dead(status_summary)
    _immune = stats.get_number_immune(status_summary)

    _vulnerability_rate = _vulnerable / _pop * 100.0
    _symptomatic_rate = _symptomatic / _pop * 100
    if _deaths + _immune > 0:
        _mortality_rate = _deaths / (_deaths + _immune) * 100
    else:
        _mortality_rate = 0.0

    latest_iteration_vulnerable.text(
        f'Percent Vulnerable:  {_vulnerability_rate}')
    latest_iteration_symptomatic.text(
        f'Percent Symptomatic: {_symptomatic_rate}')
    latest_iteration_mortality.text(
        f'Mortality Rate:      {_mortality_rate}')

stack_histories = []
stack_labels = []
for _status in range(7):
    stack_histories.append(status_history[_status])
    stack_labels.append(INFECTION_STATES[_status])

fig = plt.figure()
plt.stackplot(range(max_days), stack_histories, labels=stack_labels)
plt.legend()

st.pyplot(fig)
    

