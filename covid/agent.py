from __future__ import annotations

import numpy as np

from dataclasses import dataclass
from typing import Tuple

from .info import PROGRESS_PARAMETERS

def get_agent_status(agent: Agent) -> int:
        return agent.status

class Agent:
    class_counter = 0
    def __init__(self, infection_threshold: float = 0.5,
                 progress_statistics: dict = PROGRESS_PARAMETERS) -> None:

        self.status = 0
        self.days_at_status = 0

        self.infection_threshold = infection_threshold

        self.probability_symptomatic = progress_statistics['probability_symptomatic']
        self.probability_critical = progress_statistics['probability_critical']
        self.probability_death = progress_statistics['probability_death']
        self.max_days_asymptomatic = progress_statistics['max_days_asymptomatic']
        self.max_days_symptomatic = progress_statistics['max_days_symptomatic']
        self.max_days_critical = progress_statistics['max_days_critical']
        self.max_days_recovery = progress_statistics['max_days_recovery']
        self.average_days_immune = progress_statistics['average_days_immune']

        self.id = Agent.class_counter
        Agent.class_counter += 1

    def advance_day(self, contacts: float) -> None:
        if self.status == 0:
            self.status = self._vulnerable(contacts, self.infection_threshold)
            self.days_at_status = 0
        else:
            self.status, self.days_at_status = self._infection(
                self.status, self.days_at_status)

    def force_infection(self) -> None:
        self.status = 1

    def _vulnerable(self, contacts: float, infection_threshold: float) -> int:
        if contacts >= infection_threshold:
            return 1
        else:
            return 0

    @staticmethod
    def _status_changer(current_status: int, days_at_status: int, status_change: float, next_status: float, 
                    max_days: int, progress_probability: float, advance_state: int) -> Tuple[int, int]:
        if status_change <= (days_at_status / max_days):
            days_at_status = 0
            if next_status < progress_probability:
                status = advance_state
            else:
                status = 4
        else:
            status = current_status
            days_at_status += 1
                
        return status, days_at_status

    def _infection(self, current_status: int, days_at_status: int) -> Tuple[int, int]:
        
        status_change = np.random.rand()
        next_status = np.random.rand()
        
        if current_status == 7:
            return 7, 0
        elif current_status == 1:
            return self._status_changer(
                current_status, days_at_status, status_change, next_status,
                self.max_days_asymptomatic, self.probability_symptomatic, 2)
        elif current_status == 2:
            return self._status_changer(
                current_status, days_at_status, status_change, next_status,
                self.max_days_symptomatic, self.probability_critical, 3)
        elif current_status == 3:
            return self._status_changer(
                current_status, days_at_status, status_change, next_status,
                self.max_days_critical, self.probability_death, 6)
        elif current_status == 4:
            return self._status_changer(
                current_status, days_at_status, status_change, next_status,
                self.max_days_recovery, 1.0, 5)
        elif current_status == 5:
            if status_change < (1 / self.average_days_immune):
                return 0, 0
            else:
                return 5, days_at_status + 1
        else:
            return 6, days_at_status + 1