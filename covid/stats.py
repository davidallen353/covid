from typing import Dict, List

def _counter(status_summary: Dict[int, int], valid_statuses: List[int]) -> float:
    count = 0

    for status in valid_statuses:
        count += status_summary[status]

    return float(count)

def get_population_size(status_summary: Dict[int, int]) -> float:
    valid_statuses = [0, 1, 2, 3, 4, 5, 6]

    return _counter(status_summary, valid_statuses)

def get_number_vulnerable(status_summary: Dict[int, int]) -> float:
    valid_statuses = [0, ]
    return _counter(status_summary, valid_statuses)

def get_number_immune(status_summary: Dict[int, int]) -> float:
    valid_statuses = [5, ]
    return _counter(status_summary, valid_statuses)

def get_number_dead(status_summary: Dict[int, int]) -> float:
    valid_statuses = [6, ]
    return _counter(status_summary, valid_statuses)

def get_number_infected(status_summary: Dict[int, int]) -> float:
    valid_statuses = [1, 2, 3, 4]
    return _counter(status_summary, valid_statuses)

def get_number_symptomatic(status_summary: Dict[int, int]) -> float:
    valid_statuses = [2, 3, 4]
    return _counter(status_summary, valid_statuses)