
INFECTION_STATES = {0: 'vulnerable',
                    1: 'infected, asymptomatic',
                    2: 'infected, symptomatic',
                    3: 'infected, critical',
                    4: 'infected, recovering',
                    5: 'immune',
                    6: 'dead',
                    7: 'spacer',}

SPREAD_PROBABILITY = {0: 0,
                      1: 0.5,
                      2: 0.9,
                      3: 0.9,
                      4: 0.5,
                      5: 0.1,
                      6: 0,
                      7: 0}

PROGRESS_PARAMETERS = {
    'probability_symptomatic': 0.1,
    'probability_critical': 0.3,
    'probability_death': 0.4,
    'max_days_asymptomatic': 21,
    'max_days_symptomatic': 7,
    'max_days_critical': 7,
    'max_days_recovery': 7,
    'average_days_immune': 300,
    }