import numpy as np

from typing import Dict

from .agent import Agent, get_agent_status
from .info import SPREAD_PROBABILITY, PROGRESS_PARAMETERS

class Population:

    def __init__(self, size: int, progress_statistics: dict = PROGRESS_PARAMETERS) -> None:
        self._size = size

        _agents = []
        for _row in range(size):
            _agents.append([])
            for _col in range(size):
                _agents[-1].append(Agent(
                    infection_threshold=np.random.rand(),
                    progress_statistics=progress_statistics))
        self.agents = np.array(_agents)

    def start_infection(self, number_infected: int = 1) -> None:

        for _ in range(number_infected):
            _i = int(np.random.rand() * self._size)
            _j = int(np.random.rand() * self._size)
            self.agents[_i, _j].force_infection()

    def advance_day(self, notional_distance: float) -> None:
        self.shuffle_population(notional_distance)
        self.apply_contacts(self.calculate_contacts())

    def get_id_array(self) -> np.ndarray:
        _ids = []
        for _row in self.agents:
            _ids.append([])
            for _agent in _row:
                _ids[-1].append(_agent.id)

        return np.array(_ids)

    def remove_agents(self, number_to_remove: int) -> None:
        if number_to_remove >= self._size ** 2:
            raise ValueError('Removing too many')

        remove_list = []

        while len(remove_list) < number_to_remove:
            _x_remove = int(np.round(np.random.rand() * (self._size - 1)))
            _y_remove = int(np.round(np.random.rand() * (self._size - 1)))

            _candidate = [_x_remove, _y_remove]

            if _candidate not in remove_list:
                remove_list.append(_candidate)

        for _removal in remove_list:
            print(_removal)
            self.agents[_removal[0], _removal[1]].status = 7

    def get_status_array(self) -> np.ndarray:

        _statuses = []
        for _row in self.agents:
            _statuses.append([])
            for _agent in _row:
                _statuses[-1].append(_agent.status)

        return np.array(_statuses)

    def get_status_summary(self) -> Dict[int, int]:

        return {_status: np.sum(self.get_status_array() == _status) 
                for _status in range(7)}

    def shuffle_population(self, notional_distance: float = 3.0) -> None:
        self.agents = shuffle_array(self.agents, notional_distance=notional_distance)

    def calculate_contacts(self) -> np.ndarray:
        contacts = np.zeros(np.shape(self.agents))

        for _i in range(self._size):
            for _j in range(self._size):
                _agent_status = self.agents[_i, _j].status
                _contacts = SPREAD_PROBABILITY[_agent_status]

                for _ii in [_i - 1, _i, _i + 1]:
                    for _jj in [_j - 1, _j, _j + 1]:
                        if (_ii < 0) or (_jj < 0) or (_ii > self._size - 1) or (_jj > self._size - 1):
                            pass
                        else:
                            contacts[_ii, _jj] += _contacts

        return contacts

    def apply_contacts(self, contacts: np.ndarray) -> None:
        for _i in range(self._size):
            for _j in range(self._size):
                self.agents[_i, _j].advance_day(contacts[_i, _j])


def shuffle_array(input_array: np.ndarray, notional_distance: float = 3.0) -> np.ndarray:

    row_sort_array = []
    col_sort_array = []
    
    input_array_size = np.shape(input_array)
    
    for i in range(input_array_size[0]):
        row_sort_array.append([])
        col_sort_array.append([])
        for j in range(input_array_size[1]):
            row_sort_array[-1].append(
                (i + np.random.rand() * 2 * notional_distance) - notional_distance)
            col_sort_array[-1].append(
                (j + np.random.rand() * 2 * notional_distance) - notional_distance)

    for i in range(input_array_size[0]):
        input_array[:, i] = input_array[
            np.argsort(row_sort_array, axis=0)[:, i], i]

    for j in range(input_array_size[1]):
        input_array[j, :] = input_array[
            j, np.argsort(col_sort_array, axis=1)[j, :]]

    return input_array